module Main

import System -- getArgs

main : IO ()
main = do
    args <- getArgs
    case args of
        [] => putStrLn ("This should not happens.")
        (a0 :: []) => putStrLn ("You called: " ++ a0 ++ "\nUsage: first arg is used, others are ignored")
        (_ :: (a1 :: _)) => putStrLn ("Hello, " ++ a1)
    -- https://www.idris-lang.org/docs/1.0/prelude_doc/docs/Prelude.List.html
